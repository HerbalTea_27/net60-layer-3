﻿using Zenith.Dao.Interfaces.Req.Account;

namespace Service.Interface
{

    /// <summary>
    /// 描述：账户相关接口
    /// 作者：杨钊
    /// 时间：2023-2-21
    /// </summary>
    public interface IAccount
    {
        // <summary>
        /// 描述：登录接口
        /// 作者：杨钊
        /// 时间：2023-2-21
        /// </summary>
        /// <param name="req">入参实体</param>
        /// <returns></returns>
        Task<object> SignInAsync(SignInReq req);
    }
}
