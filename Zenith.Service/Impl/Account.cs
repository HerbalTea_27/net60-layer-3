﻿using Zenith.Dao.Interface;
using EntityFrameworkCore.Mysql;
using Microsoft.EntityFrameworkCore;
using Service.Interface;
using System.Security.Claims;
using Zenith.Dao.Interfaces.Req.Account;
using EntityFrameworkCore.MigrationTool;
using EntityFrameworkCore.Supreme;

namespace Service.Impl
{
    public class Account : IAccount
    {
        private readonly IDaoAccount _daoAccount;

        public Account(IDaoAccount daoAccount)
        {
            _daoAccount = daoAccount;
        }
        public async Task<object> SignInAsync(SignInReq req)
        {
            try
            {
                if (req.UserId.IsNullOrWhiteSpace() || req.UserPwd.IsNullOrWhiteSpace())
                {
                    return ResultData.Fail("账号密码不能为空！");
                }
                var data = await _daoAccount.SignInQueryable(req)
                    .Where(x => x.UserId == req.UserId)
                    .Where(x => x.UserPwd == req.UserPwd)
                    .ToListAsync();
                if (data == null || data.Count() < 1)
                {
                    return ResultData.Fail("账号密码错误！");
                }
                var RoleIds = string.Join(",", data.Select(x => x.RoleID));
                var userInfo = new UserInfo()
                {
                    ID = data.FirstOrDefault().ID,
                    UserName = data.FirstOrDefault().UserName,
                    RoleIds = RoleIds,
                };
                var token = JwtHelper.CreateJwtToken(new List<Claim>()
                {
                    new Claim("ID",userInfo.ID.ToString()),
                    new Claim("Name",userInfo.UserName),
                    new Claim("RoleIds",userInfo.RoleIds)
                });
                Log.Info("【" + userInfo.ID + "】登录成功");
                return new
                {
                    user = userInfo,
                    token,
                }.Success();
            }
            catch (Exception ex)
            {
                throw new JrcException(ex.Message, 200);
            }

        }
    }
}
