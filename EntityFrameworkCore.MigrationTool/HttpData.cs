﻿using Microsoft.AspNetCore.Mvc.Filters;
using Newtonsoft.Json;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using UAParser;

namespace EntityFrameworkCore.MigrationTool
{
    public static class HttpData
    {
        public static ClientInfo GetBrowser(this ActionExecutingContext context)
        {
            var browser = context.HttpContext.Request.Headers["User-Agent"];
            var parser = Parser.GetDefault();
            var clientInfo = parser.Parse(browser);
            return clientInfo;
        }

        public static string GetIp(this ActionExecutingContext context)
        {
            var ip = context.HttpContext.Connection.RemoteIpAddress.ToString();
            return ip;
        }
    }
}
