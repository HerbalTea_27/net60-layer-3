﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityFrameworkCore.MigrationTool
{
    /// <summary>
    /// 描述：JrcException错误处理
    /// 作者：杨钊
    /// 时间：2023-2-21
    /// </summary>
    public class JrcException : NotImplementedException
    {
        public JrcException(string message, int Code)
        {
            Log.Error(message);
        }
    }
}
