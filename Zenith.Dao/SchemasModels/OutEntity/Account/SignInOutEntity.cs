﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zenith.Dao.SchemasModels.OutEntity.Account
{
    /// <summary>
    /// 描述：登录Out
    /// 作者：杨钊
    /// 时间：2023-2-21
    /// </summary>
    public class SignInOutEntity
    {
        /// <summary>
        /// 用户账号
        /// </summary>
        public string UserId { get; set; }

        /// <summary>
        /// 密码
        /// </summary>
        public string UserPwd { get; set; }

        /// <summary>
        /// 用户名
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// 角色名称
        /// </summary>
        public string RoleName { get; set; }

        /// <summary>
        /// 主键ID
        /// </summary>
        public long ID { get; set; }

        /// <summary>
        /// 角色主键ID
        /// </summary>
        public long RoleID { get; set; }
    }
}
