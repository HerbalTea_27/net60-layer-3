﻿using EntityFrameworkCore.Mysql.Models;
using EntityFrameworkCore.Mysql.RepositoryBase;
using Zenith.Dao.Interfaces.Req.Account;
using Zenith.Dao.SchemasModels.OutEntity.Account;
using Zenith.Dao.Interfaces;
using Zenith.Dao.Interface;

namespace Zenith.Dao.Impl
{
    /// <summary>
    /// 描述：Dao账户类
    /// 作者：杨钊
    /// 时间：2023-2-21
    /// </summary>
    public class DaoAccount : IDaoAccount
    {
        private readonly IRepository<Sys_User> _entityUser;
        private readonly IRepository<Sys_UserRole> _entityUserRole;
        private readonly IRepository<Sys_Role> _entityRole;
        public DaoAccount(IRepository<Sys_User> entityUser,
            IRepository<Sys_UserRole> entityUserRole,
            IRepository<Sys_Role> entityRole)
        {
            _entityUser = entityUser;
            _entityUserRole = entityUserRole;
            _entityRole = entityRole;
        }
        /// <inheritdoc/>
        public IQueryable<SignInOutEntity> SignInQueryable(SignInReq req)
        {

            var userQueryable = _entityUser.AsQueryable
                    .Where(x => x.UserId == req.UserId)
                    .Where(x => x.UserPwd == req.UserPwd);
            var dataQueryable = from su in userQueryable
                                join sur in _entityUserRole.AsQueryable
                                on su.ID equals sur.UserID
                                join sr in _entityRole.AsQueryable
                                on sur.RoleID equals sr.ID
                                select new SignInOutEntity
                                {
                                    UserId = su.UserId,
                                    UserPwd = su.UserPwd,
                                    UserName = su.UserName,
                                    RoleName = sr.RoleName,
                                    ID = sur.UserID,
                                    RoleID = sur.RoleID
                                };

            return dataQueryable;
        }
    }
}
