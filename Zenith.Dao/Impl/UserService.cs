﻿using Zenith.Dao.Interfaces;
using EntityFrameworkCore.Supreme;
using Microsoft.AspNetCore.Http;
using Zenith.Dao.Interface;

namespace Zenith.Dao.Impl
{
    /// <summary>
    /// 描述：Dao层用户操作类
    /// 作者：杨钊
    /// 时间：2023-2-21
    /// </summary>
    public class UserService : IUserService
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        public UserService(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        public UserInfo UserInfo => _httpContextAccessor.VerifyJwtToken().Item2;
        public bool JwtTokenBool => _httpContextAccessor.VerifyJwtToken().Item1;
        public long ID => _httpContextAccessor.VerifyJwtToken().Item2.ID;
    }
}
