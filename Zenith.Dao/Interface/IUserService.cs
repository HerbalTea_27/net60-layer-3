﻿using EntityFrameworkCore.Supreme;

namespace Zenith.Dao.Interface
{
    public interface IUserService
    {
        /// <summary>
        /// 用户信息
        /// </summary>
        UserInfo UserInfo { get; }
        /// <summary>
        /// 用户ID
        /// </summary>
        long ID{ get; }
        /// <summary>
        /// token是否验证成功
        /// </summary>
        bool JwtTokenBool { get; }
    }
}
