﻿using Zenith.Dao.SchemasModels.OutEntity.Account;
using Zenith.Dao.Interfaces.Req.Account;

namespace Zenith.Dao.Interface
{
    public interface IDaoAccount
    {
        /// <summary>
        /// 描述：登录Queryable获取
        /// 作者：杨钊
        /// 时间：2023-2-21
        /// </summary>
        /// <param name="req">查询参数</param>
        /// <returns></returns>
        IQueryable<SignInOutEntity> SignInQueryable(SignInReq req);
    }
}
