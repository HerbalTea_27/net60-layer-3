﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityFrameworkCore.Supreme.SchemasModels
{
    public class Page
    {
        public int Index { get; set; } = 1;
        public int Num { get; set; } = 15;
    }
}
