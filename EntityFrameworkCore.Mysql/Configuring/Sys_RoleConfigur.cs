﻿using EntityFrameworkCore.Mysql.RepositoryBase;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace EntityFrameworkCore.Mysql.Configuring
{
    /// <summary>
    /// 描述：Sys_RoleConfigur
    /// 作者：杨钊
    /// 时间：2023-3-20
    /// </summary>
    public class Sys_RoleConfigur : IEntityTypeConfiguration<Sys_Role>
    {
        /// <summary>
        /// 配置
        /// </summary>
        /// <param name="builder"></param>
        public void Configure(EntityTypeBuilder<Sys_Role> builder)
        {
            builder.GetDefault();
            builder.Property(t => t.RoleName).IsRequired().HasMaxLength(25);
            builder.Property(t => t.RoleDesc).HasMaxLength(1000);
            builder.Property(t => t.IsEnable);
        }
    }
}

