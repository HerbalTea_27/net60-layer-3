﻿using EntityFrameworkCore.Mysql.RepositoryBase;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace EntityFrameworkCore.Mysql.Configuring
{
    /// <summary>
    /// 描述：Sys_UserRoleConfigur
    /// 作者：杨钊
    /// 时间：2023-3-20
    /// </summary>
    public class Sys_UserRoleConfigur : IEntityTypeConfiguration<Sys_UserRole>
    {
        /// <summary>
        /// 配置
        /// </summary>
        /// <param name="builder"></param>
        public void Configure(EntityTypeBuilder<Sys_UserRole> builder)
        {
            builder.GetDefault();
            builder.Property(t => t.RoleID).IsRequired().HasMaxLength(20);
            builder.Property(t => t.UserID).IsRequired().HasMaxLength(20);
        }
    }
}

