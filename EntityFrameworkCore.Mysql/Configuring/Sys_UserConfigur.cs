﻿using EntityFrameworkCore.Mysql.RepositoryBase;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace EntityFrameworkCore.Mysql.Configuring
{
    /// <summary>
    /// 描述：Sys_UserClaimConfigur
    /// 作者：杨钊
    /// 时间：2023-3-20
    /// </summary>
    public class Sys_UserConfigur : IEntityTypeConfiguration<Sys_User>
    {
        /// <summary>
        /// 配置
        /// </summary>
        /// <param name="builder"></param>
        public void Configure(EntityTypeBuilder<Sys_User> builder)
        {
            builder.GetDefault();
            builder.Property(t => t.UserId).IsRequired().HasMaxLength(25);
            builder.Property(t => t.UserName).IsRequired().HasMaxLength(25);
            builder.Property(t => t.UserPwd).IsRequired().HasMaxLength(25);
            builder.Property(t => t.IsEnable);
        }
    }
}

