﻿namespace EntityFrameworkCore.Mysql
{
    /// <summary>
    /// 公共字段
    /// </summary>
    public class BaseEntity
    {
        /// <summary>
        /// 主键
        /// </summary>
        public int MajorKeyId { get; set; }
        /// <summary>
        /// ID
        /// </summary>
        public long ID { get; set; }
        /// <summary>
        /// 是否删除
        /// </summary>
        public bool IsDelete { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateTime { get; set; }
        /// <summary>
        /// 修改时间
        /// </summary>
        public DateTime? UpdateTime { get; set; }
        /// <summary>
        /// 创建人ID
        /// </summary>
        public long CreateUserID { get; set; }
        /// <summary>
        /// 修改人ID
        /// </summary>
        public long? UpdateUserID { get; set; }
    }
}
