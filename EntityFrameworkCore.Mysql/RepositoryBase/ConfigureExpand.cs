﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace EntityFrameworkCore.Mysql.RepositoryBase
{
    /// <summary>
    /// 描述：ConfigureExpand
    /// 作者：杨钊
    /// 时间：2023-3-20
    /// </summary>
    public static class ConfigureExpand
    {
        /// <summary>
        /// 获取默认配置
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="builder"></param>
        public static void GetDefault<T>(this EntityTypeBuilder<T> builder) where T : BaseEntity
        {
            builder.ToTable(typeof(T).Name.ToLower());
            builder.HasKey(t => t.MajorKeyId);
            builder.HasIndex(u => u.ID).IsUnique();
            builder.Property(t => t.MajorKeyId).HasColumnOrder(1).ValueGeneratedOnAdd();
            builder.Property(t => t.ID).HasMaxLength(20).HasColumnOrder(2);
            builder.Property(t => t.IsDelete);
            builder.Property(t => t.CreateTime).HasMaxLength(6);
            builder.Property(t => t.UpdateTime).HasMaxLength(6);
            builder.Property(t => t.CreateUserID).HasMaxLength(20);
            builder.Property(t => t.UpdateUserID).HasMaxLength(20);
        }

        /// <summary>
        /// MySqlDouble
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="builder"></param>
        /// <param name="precision"></param>
        /// <param name="scale"></param>
        public static PropertyBuilder<T> HasPrecisionDouble<T>(this PropertyBuilder<T> builder, int precision, int scale)
        {
            return builder.HasColumnType("double(" + precision + "," + scale + ")");
        }
    }
}
