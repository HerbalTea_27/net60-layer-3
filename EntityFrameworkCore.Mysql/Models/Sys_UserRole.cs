using System;
using System.Collections.Generic;

namespace EntityFrameworkCore.Mysql.Models
{
    /// <summary>
    /// 用户角色表
    /// </summary>
    public partial class Sys_UserRole : BaseEntity
    {
        /// <summary>
        /// 角色ID
        /// </summary>
        public long RoleID { get; set; }
        /// <summary>
        /// 用户ID
        /// </summary>
        public long UserID { get; set; }
    }
}