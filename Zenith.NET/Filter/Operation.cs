﻿using EntityFrameworkCore.Supreme;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Mvc.Filters;
using EntityFrameworkCore.MigrationTool;

namespace Zenith.NetCore.Filter
{
    /// <summary>
    /// 过滤器
    /// </summary>
    public class Operation : ActionFilterAttribute
    {
        /// <summary>
        /// Action方法调用之前执行
        /// </summary>
        /// <param name="context"></param>
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            var authorization = context.HttpContext.Request.Headers.Authorization.ToString();
            UserInfo? userInfo = null;
            if (!string.IsNullOrEmpty(authorization) && authorization.StartsWith("Bearer "))
            {
                if (AppHelper.ReadAppSettings("Authentication") == "IdentityServer")
                {

                    var items = JwtHelper.VerifyIdentity(context.HttpContext.User.Claims);
                    userInfo = items.Item2;
                }
                else
                {

                    var items = JwtHelper.VerifyJwtToken(authorization["Bearer ".Length..]);
                    userInfo = items.Item2;
                }
            }
            var descriptor = context.ActionDescriptor as ControllerActionDescriptor;
            Log.InfoOperation(context.GetIp(), userInfo?.ID, context.ActionArguments, descriptor.ActionName, descriptor.ControllerName);

        }
    }
}
